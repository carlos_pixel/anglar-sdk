import { Component } from '@angular/core';


declare global {
  interface Window { PixelPay: any; }
}
window.PixelPay = window.PixelPay || {};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angularnine';


  constructor() {
    // window.PixelPay.setup('', '', '');
    // console.log(this.createOrder());
  }

  createOrder() {
    var order = window.PixelPay.newOrder();

    order.setOrderID('AB1234');
    order.setFullName('John Doe');
    order.setEmail('johndoe@email.test');
    order.setAmount(1.99);

    console.log(order);
  }

}
